<?php
declare(strict_types=1);

namespace Vipkwd\Utils\Qrcode;

use Vipkwd\Utils\Libs\Qrcode\{QRtools, QRencode, Enum, QRcode as PHPQRcode};
use \Exception;

QRtools::markTime('start');
class QRencdr extends QRencode
{

    public $cmyk = false;
    public $size = 3;
    public $margin = 4;

    public static function factory($level = Enum::QR_ECLEVEL_L, $size = 3, $margin = 4, $back_color = 0xFFFFFF, $front_color = 0x000000, $cmyk = false)
    {
        $enc = new QRencdr();
        $enc->size = $size;
        $enc->margin = $margin;
        $enc->front_color = $front_color;
        $enc->back_color = $back_color;
        $enc->cmyk = $cmyk;

        switch ($level . '') {
            case '0':
            case '1':
            case '2':
            case '3':
                $enc->level = $level;
                break;
            case 'l':
            case 'L':
                $enc->level = Enum::QR_ECLEVEL_L;
                break;
            case 'm':
            case 'M':
                $enc->level = Enum::QR_ECLEVEL_M;
                break;
            case 'q':
            case 'Q':
                $enc->level = Enum::QR_ECLEVEL_Q;
                break;
            case 'h':
            case 'H':
                $enc->level = Enum::QR_ECLEVEL_H;
                break;
        }

        return $enc;
    }

    public function encodePNG($intext, $outfile = false, $saveandprint = false, $style = false)
    {
        return $this->_creater('png', $intext, $outfile, $saveandprint, $style);
    }

    public function encodeSVG($intext, $outfile = false, $saveandprint = false, $style = false)
    {
        return $this->_creater('svg', $intext, $outfile, $saveandprint, $style);
    }
    public function encodeEPS($intext, $outfile = false, $saveandprint = false, $style = false)
    {
        return $this->_creater('eps', $intext, $outfile, $saveandprint, $style);
    }
    private function _creater(string $type, $intext, $outfile = false, $saveandprint = false, $style = false)
    {
        try {
            ob_start();
            $tab = $this->encode($intext, $outfile);
            $err = ob_get_contents();
            ob_end_clean();
            if ($err != '')
                QRtools::log($outfile, $err);

            $maxSize = (int) (Enum::QR_PNG_MAXIMUM_SIZE / (count($tab) + 2 * $this->margin));
            switch ($type) {
                case "png":
                    QRimg::png(
                        $tab,
                        $outfile,
                        min(
                            max(
                                1,
                                $this->size
                            ),
                            $maxSize
                        ),
                        $this->margin,
                        $saveandprint,
                        $this->back_color,
                        $this->front_color,
                        $style
                    );
                    break;
                case "svg":
                    QRvct::svg(
                        $tab,
                        $outfile,
                        min(
                            max(
                                1,
                                $this->size
                            ),
                            $maxSize
                        ),
                        $this->margin,
                        $saveandprint,
                        $this->back_color,
                        $this->front_color,
                        $style
                    );
                    break;
                case "eps":
                    PHPQRcode::eps(
                        $tab,
                        $outfile,
                        $this->level,
                        min(
                            max(
                                1,
                                $this->size
                            ),
                            $maxSize
                        ),
                        $this->margin,
                        $saveandprint,
                        $this->back_color,
                        $this->front_color,
                        $this->cmyk
                    );
                    break;
            }

        } catch (Exception $e) {
            QRtools::log($outfile, sprintf("%s(%d) %s\r\n", $e->getFile(), $e->getLine(), $e->getMessage()));
        }
    }
}