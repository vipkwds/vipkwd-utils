<?php
declare(strict_types=1);

namespace Vipkwd\Utils\Qrcode;

use Vipkwd\Utils\Libs\Qrcode\{QRtools, QRvect};

QRtools::markTime('start');
class QRvct extends QRvect
{
    //----------------------------------------------------------------------
    public static function svg($frame, $filename = false, $pixelPerPoint = 4, $outerFrame = 4, $saveandprint = FALSE, $back_color = 0xFFFFFF, $front_color = 0x000000, $style = false)
    {
        $vect = self::vectSVG($frame, $pixelPerPoint, $outerFrame, $back_color, $front_color, $style);

        if ($filename === false) {
            header("Content-Type: image/svg+xml");
            //header('Content-Disposition: attachment, filename="qrcode.svg"');
            echo $vect;
        } else {
            if ($saveandprint === true) {
                QRtools::save($vect, $filename);
                header("Content-Type: image/svg+xml");
                //header('Content-Disposition: filename="'.$filename.'"');
                echo $vect;
            } else {
                QRtools::save($vect, $filename);
            }
        }
    }


    //----------------------------------------------------------------------
    private static function vectSVG($frame, $pixelPerPoint = 4, $outerFrame = 4, $back_color = 0xFFFFFF, $front_color = 0x000000, $style = false)
    {
        $h = count($frame);
        $w = strlen($frame[0]);
        $dot = $pixelPerPoint; // Pixels per dot
        $radius = $dot / 2;

        $imgW = $w + $outerFrame * 2;
        $imgH = $h + $outerFrame * 2;

        $output = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
            '<svg version="1.1" baseProfile="full"  width="' . $imgW * $pixelPerPoint . '" height="' . $imgH * $pixelPerPoint . '" viewBox="0 0 ' . $imgW * $pixelPerPoint . ' ' . $imgH * $pixelPerPoint . '"
         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ev="http://www.w3.org/2001/xml-events">' . "\n" .
            '<desc></desc>' . "\n";

        $output = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
            '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20010904//EN" "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">' . "\n" .
            '<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" xmlns:xlink="http://www.w3.org/1999/xlink" width="' . $imgW * $pixelPerPoint . '" height="' . $imgH * $pixelPerPoint . '" viewBox="0 0 ' . $imgW * $pixelPerPoint . ' ' . $imgH * $pixelPerPoint . '">' . "\n" .
            '<desc></desc>' . "\n";

        if (!empty($back_color)) {
            // $backgroundcolor = str_pad(dechex($back_color), 6, "0", STR_PAD_LEFT);
            $backgroundcolor = QRtools::colorToHex($back_color);
            $output .= '<rect width="' . $imgW * $pixelPerPoint . '" height="' . $imgH * $pixelPerPoint . '" fill="#' . $backgroundcolor . '" cx="0" cy="0" />' . "\n";
        }

        $plusline = (int) min(max(($dot / 8), 1), 8);
        $polypoints1 = '0,0 ' . $dot . ',0 0,' . $dot;
        $polypoints2 = '0,' . $dot . ' ' . $dot . ',0 ' . $dot . ',' . $dot;

        // $front_color = str_pad(dechex($front_color), 6, "0", STR_PAD_LEFT);
        $front_color = QRtools::colorToHex($front_color);
        $lighter = adjustBrightness(strval($front_color), 0.5);

        $output .=
            '<defs>' . "\n" .
            '<rect id="p" width="' . $pixelPerPoint . '" height="' . $pixelPerPoint . '" />' . "\n" .
            '<circle id="circle" r="' . $radius . '" stroke-width="0" fill="' . $front_color . '" />' . "\n" .
            '<rect id="hor" width="' . $pixelPerPoint . '" height="' . ($plusline * 2) . '" />' . "\n" .
            '<rect id="ver" width="' . ($plusline * 2) . '" height="' . $pixelPerPoint . '" />' . "\n" .
            '<polygon id="3d1" points="' . $polypoints1 . '" stroke-width="0" fill="' . $front_color . '" />' . "\n" .
            '<polygon id="3d2" points="' . $polypoints2 . '" stroke-width="0" fill="' . $lighter . '" />' . "\n" .
            '</defs>' . "\n" .
            '<g fill="#' . $front_color . '">' . "\n";

        switch ($style) {
            case 'plus':
                for ($i = 0; $i < $h; $i++) {
                    for ($j = 0; $j < $w; $j++) {
                        if ($frame[$i][$j] == '1') {
                            // Plus
                            $y = ($i + $outerFrame) * $pixelPerPoint;
                            $x = ($j + $outerFrame) * $pixelPerPoint;
                            $output .= '<use x="' . $x . '" y="' . ($y + $pixelPerPoint / 2 - $plusline) . '" xlink:href="#hor" />' . "\n";
                            $output .= '<use x="' . ($x + $pixelPerPoint / 2 - $plusline) . '" y="' . $y . '" xlink:href="#ver" />' . "\n";
                        }
                    }
                }
                break;

            case 'circle':
                for ($i = 0; $i < $h; $i++) {
                    for ($j = 0; $j < $w; $j++) {
                        if ($frame[$i][$j] == '1') {
                            // Circle
                            $y = ($i + $outerFrame) * $pixelPerPoint + $radius;
                            $x = ($j + $outerFrame) * $pixelPerPoint + $radius;
                            $output .= '<use x="' . $x . '" y="' . $y . '" xlink:href="#circle" />' . "\n";
                        }
                    }
                }
                break;

            case '3d':
                for ($i = 0; $i < $h; $i++) {
                    for ($j = 0; $j < $w; $j++) {
                        if ($frame[$i][$j] == '1') {
                            // Circle
                            $y = ($i + $outerFrame) * $pixelPerPoint;
                            $x = ($j + $outerFrame) * $pixelPerPoint;
                            $output .= '<use x="' . $x . '" y="' . $y . '" xlink:href="#3d1" />' . "\n";
                            $output .= '<use x="' . $x . '" y="' . $y . '" xlink:href="#3d2" />' . "\n";
                        }
                    }
                }
                break;

            default:
                for ($i = 0; $i < $h; $i++) {
                    for ($j = 0; $j < $w; $j++) {
                        if ($frame[$i][$j] == '1') {
                            // square
                            $y = ($i + $outerFrame) * $pixelPerPoint;
                            $x = ($j + $outerFrame) * $pixelPerPoint;
                            $output .= '<use x="' . $x . '" y="' . $y . '" xlink:href="#p" />' . "\n";
                        }
                    }
                }
                break;
        }

        // Convert the matrix into pixels
        // for ($i=0; $i<$h; $i++) {
        //     for ($j=0; $j<$w; $j++) {
        //         if ($frame[$i][$j] == '1') {

        //             // Circle
        //             // $y = ($i + $outerFrame) * $pixelPerPoint;
        //             // $x = ($j + $outerFrame) * $pixelPerPoint;
        //             // $output .= '<use x="'.$x.'" y="'.$y.'" xlink:href="#p" />'."\n";
        //             // $output .= '<use x="'.$x.'" y="'.$y.'" xlink:href="#circle" />'."\n";

        //             // Plus
        //             $y = ($i + $outerFrame) * $pixelPerPoint;
        //             $x = ($j + $outerFrame) * $pixelPerPoint;
        //             $output .= '<use x="'.$x.'" y="'.($y + $pixelPerPoint/2-$plusline).'" xlink:href="#hor" />'."\n";
        //             $output .= '<use x="'.($x + $pixelPerPoint/2-$plusline).'" y="'.$y.'" xlink:href="#ver" />'."\n";
        //         }
        //     }
        // }
        $output .= '</g>' . "\n" . '</svg>';

        return $output;
    }
}