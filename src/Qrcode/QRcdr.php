<?php
declare(strict_types=1);

namespace Vipkwd\Utils\Qrcode;

use Vipkwd\Utils\Libs\Qrcode\{QRtools, Enum, QRcode as PHPQRcode};

QRtools::markTime('start');

class QRcdr extends PHPQRcode
{
    /**
     * Create PNG
     */
    public static function png($text, $outfile = false, $level = Enum::QR_ECLEVEL_L, $size = 3, $margin = 4, $saveandprint = false, $back_color = 0xFFFFFF, $front_color = 0x000000, $style = false)
    {
        $enc = QRencdr::factory($level, $size, $margin, $back_color, $front_color);
        return $enc->encodePNG($text, $outfile, $saveandprint, $style);
    }

    /**
     * Create SVG
     */
    public static function svg($text, $outfile = false, $level = Enum::QR_ECLEVEL_L, $size = 3, $margin = 4, $saveandprint = false, $back_color = 0xFFFFFF, $front_color = 0x000000, $style = false)
    {
        $enc = QRencdr::factory($level, $size, $margin, $back_color, $front_color);
        return $enc->encodeSVG($text, $outfile, $saveandprint, $style);
    }

    // QRcdr::eps() <-- QRcode::eps() || QRencdr::factory()->encodeEPS()
}
