<?php
/**
 * @name JsonDB
 * @author vipkwd <service@vipkwd.com>
 * @link https://github.com/wxy545812093/vipkwd-phputils
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @copyright The PHP-Tools
 */

declare(strict_types=1);

namespace Vipkwd\Utils\Db;

use Vipkwd\Utils\Libs\Json\JsonDb;

class Json
{
    use JsonDb;
    // 静态实例化
    static function instance(array $options = [])
    {
        return new static($options);
    }
}