<?php
/**
 * @name AES-ECB 加密解密算法
 * @author vipkwd <service@vipkwd.com>
 * @link https://github.com/wxy545812093/vipkwd-phputils
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @copyright The PHP-Tools
 */
declare(strict_types=1);

namespace Vipkwd\Utils\Libs\Crypt;

use Vipkwd\Utils\Libs\Crypt\Traits;
use \Exception;

class AesECB
{
    use Traits;
    private static $_ivLength = 16;

    // AES-128-ECB,
    // AES-192-ECB,
    // AES-256-ECB,

    // AES-128-CBC,
    // AES-192-CBC,
    // AES-256-CBC,

    // AES-128-OFB,
    // AES-192-OFB,
    // AES-256-OFB,

    // AES-128-CFB,
    // AES-192-CFB,
    // AES-256-CFB,

    // AES-128-CTR,
    // AES-192-CTR,
    // AES-256-CTR
    private static $_modeType = "AES-256-ECB"; //前端JS库只与后端128位算法互通(AES-128-CBC即keyLen=16)

    // https://www.jianshu.com/p/54a027ed96f8
    private static $_keyLength = 16;
    private static $_mode_supports = [16 => "AES-128-ECB", 24 => "AES-192-ECB", 32 => "AES-256-ECB"];
}