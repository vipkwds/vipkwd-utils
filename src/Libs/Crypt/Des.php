<?php
/**
 * @name 3DES-CBC 加密解密算法
 * @author vipkwd <service@vipkwd.com>
 * @link https://github.com/wxy545812093/vipkwd-phputils
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @copyright The PHP-Tools
 */
declare(strict_types = 1);

namespace Vipkwd\Utils\Libs\Crypt;

use Vipkwd\Utils\Libs\Crypt\Traits;
use \Exception;

class Des
{
    use Traits;
    private static $_modeType = "des-ede3-cbc";

    // https://blog.csdn.net/mangojo/article/details/90268132

    private static $_ivLength = 8;

    /**
     * 填充
     *
     * @param $str
     * @param $blockSize
     * @return string
     * @internal param $blocksize
     */
    private function padding($str, $blockSize)
    {

        // if (strlen($str) % 4) {
        // $str = str_pad($str,strlen($str) + 4 - strlen($str) % 4, "\0");
        // }

        $pad = $blockSize - (strlen($str) % $blockSize);
        return $str . str_repeat(chr($pad), $pad);
    }


    /**
     * 去填充
     *
     * @param $str
     * @return string
     */
    private function unPadding($str)
    {
        $pad = ord($str[strlen($str) - 1]);
        if ($pad > strlen($str)) {
            return '';
        }
        return substr($str, 0, -1 * $pad);
    }
}