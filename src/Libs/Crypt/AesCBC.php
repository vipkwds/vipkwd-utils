<?php
/**
 * @name AES-CBC 加密解密算法
 * @author vipkwd <service@vipkwd.com>
 * @link https://github.com/wxy545812093/vipkwd-phputils
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @copyright The PHP-Tools
 */
declare(strict_types=1);

namespace Vipkwd\Utils\Libs\Crypt;

use Vipkwd\Utils\Libs\Crypt\Traits;
use \Exception;

class AesCBC
{
    use Traits;
    private static $_ivLength = 16;
    private static $_modeType = "AES-256-CBC"; //前端JS库只与后端128位算法互通(AES-128-CBC即keyLen=16)

    // https://www.jianshu.com/p/54a027ed96f8
    private static $_keyLength = 16;
    private static $_mode_supports = [16 => "AES-128-CBC", 24 => "AES-192-CBC", 32 => "AES-256-CBC"];
}